from flask import Blueprint, request, url_for, render_template, flash, redirect, g
from . import baze_de_date
from . import vedere_autentificare

plan_vedere_produse = Blueprint('vedere_produse', __name__, url_prefix='/produse')

def get_produse():
	if 'produse' not in g:
		with baze_de_date.get_baza_de_date_clienti().cursor() as cursor:
			cursor.execute('SELECT cod, nume, pret_unitar FROM produse')
			g.produse = cursor.fetchall()
	return g.produse
	
def get_produs(cod):
	with baze_de_date.get_baza_de_date_clienti().cursor() as cursor:
		cursor.execute('SELECT cod, nume, pret_unitar FROM produse WHERE cod = %s', (cod,))
		return cursor.fetchone()
	
@plan_vedere_produse.route('/', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_index():
	baze_de_date.get_informatii_magazin()
	get_produse()
	vedere_autentificare.get_utilizator_conectat()
	return render_template('vedere_produse/pagina_index.html')

@plan_vedere_produse.route('/adauga', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_adauga():
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	if request.method == 'POST':
		cod = request.form['cod']
		nume = request.form['nume']
		pret_unitar = request.form['pret_unitar']
		if cod is None:
			flash('Codul este necesar.')
		elif nume is None:
			flash('Numele este necesar.')
		elif pret_unitar is None:
			flash('Pretul unitar este necesar.')
		else:
			conexiune = baze_de_date.get_baza_de_date_clienti()
			with conexiune.cursor() as cursor:
				cursor.execute('INSERT INTO produse(cod, nume, pret_unitar) VALUES (%s, %s, %s)', (cod, nume, pret_unitar))
				cursor.execute('INSERT INTO stoc(cod_produs, cantitate, cantitate_suficienta) VALUES (%s, %s, %s)', (cod, 0, 1))
			conexiune.commit()
			return redirect(url_for('vedere_produse.pagina_index'))
	return render_template('vedere_produse/pagina_adauga.html')
	
@plan_vedere_produse.route('/<string:cod>/modifica', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_modifica(cod):
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	g.produs = get_produs(cod)
	if request.method == 'POST':
		nume = request.form['nume']
		pret_unitar = request.form['pret_unitar']
		if nume is None:
			flash('Numele este necesar.')
		elif pret_unitar is None:
			flash('Pretul unitar este necesar.')
		else:
			conexiune = baze_de_date.get_baza_de_date_clienti()
			with conexiune.cursor() as cursor:
				cursor.execute('UPDATE produse SET nume = %s, pret_unitar = %s WHERE cod = %s', (nume, pret_unitar, cod))
			conexiune.commit()
			return redirect(url_for('vedere_produse.pagina_index'))
	return render_template('vedere_produse/pagina_modifica.html')
	
@plan_vedere_produse.route('/<string:cod>/sterge', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_sterge(cod):
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	g.produs = get_produs(cod)
	if request.method == 'POST':
		conexiune = baze_de_date.get_baza_de_date_clienti()
		with conexiune.cursor() as cursor:
			cursor.execute('DELETE FROM produse WHERE cod = %s', (cod,))
		conexiune.commit()
		return redirect(url_for('vedere_produse.pagina_index'))
	return render_template('vedere_produse/pagina_sterge.html')
	
