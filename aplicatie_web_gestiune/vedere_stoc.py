from flask import Blueprint, request, url_for, render_template, flash, redirect, g
from . import baze_de_date
from . import vedere_autentificare
from . import vedere_produse

plan_vedere_stoc = Blueprint('vedere_stoc', __name__, url_prefix='/stoc')

def get_stoc_produs(cod_produs):
	with baze_de_date.get_baza_de_date_clienti().cursor() as cursor:
		cursor.execute('SELECT cod_produs, cantitate, cantitate_suficienta FROM stoc WHERE cod_produs = %s', (cod_produs,))
		stoc = cursor.fetchone()
		if stoc is None:
			return (cod_produs, 0, 1)
		return stoc

def get_stoc():
	if 'stoc' not in g:
		vedere_produse.get_produse()
		g.stoc = {}
		for produs in g.produse:
			g.stoc[produs[0]] = get_stoc_produs(produs[0])
	return g.stoc

@plan_vedere_stoc.route('/', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_index():
	baze_de_date.get_informatii_magazin()
	vedere_produse.get_produse()
	get_stoc()
	vedere_autentificare.get_utilizator_conectat()
	return render_template('vedere_stoc/pagina_index.html')

@plan_vedere_stoc.route('/<string:cod_produs>/modifica', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_modifica(cod_produs):
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	g.produs = vedere_produse.get_produs(cod_produs)
	g.stoc_produs = get_stoc_produs(cod_produs)
	if request.method == 'POST':
		cantitate = request.form['cantitate']
		cantitate_suficienta = request.form['cantitate_suficienta']
		if cantitate is None:
			flash('Cantitatea este necesara.')
		elif cantitate_suficienta is None:
			flash('Cantitatea suficienta este necesara.')
		else:
			conexiune = baze_de_date.get_baza_de_date_clienti()
			with conexiune.cursor() as cursor:
				cursor.execute('SELECT COUNT(cod_produs) FROM stoc WHERE cod_produs = %s', (cod_produs,))
				exista = cursor.fetchone()[0] == 1
				if exista:
					cursor.execute('UPDATE stoc SET cantitate = %s, cantitate_suficienta = %s WHERE cod_produs = %s', (cantitate,
						cantitate_suficienta, cod_produs))
				else:
					cursor.execute('INSERT INTO stoc(cod_produs, cantitate, cantitate_suficienta) VALUES (%s, %s, %s)', (cod_produs,
						cantitate, cantitate_suficienta))
			conexiune.commit()
			return redirect(url_for('vedere_stoc.pagina_index'))
	return render_template('vedere_stoc/pagina_modifica.html')
