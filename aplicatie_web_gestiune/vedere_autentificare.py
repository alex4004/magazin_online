import functools
from werkzeug.security import check_password_hash
from flask import Blueprint, request, url_for, render_template, flash, redirect, session, g
from . import baze_de_date

plan_vedere_autentificare = Blueprint('vedere_autentificare', __name__, url_prefix='/autentificare')

def get_cont_gestiune(nume):
	with baze_de_date.get_baza_de_date_gestiune().cursor() as cursor:
		cursor.execute('SELECT id, nume, hash_parola FROM conturi_gestiune WHERE nume = %s', (nume,))
		return cursor.fetchone()
		
def get_utilizator_conectat():
	id = session.get('id_cont_gestionare')
	if id is None:
		g.utilizator_conectat = None
	else:
		with baze_de_date.get_baza_de_date_gestiune().cursor() as cursor:
			cursor.execute('SELECT id, nume, hash_parola FROM conturi_gestiune WHERE id = %s', (id,))
			g.utilizator_conectat = cursor.fetchone()
	return g.utilizator_conectat

@plan_vedere_autentificare.route('/conecteaza', methods=('GET', 'POST'))
def pagina_conecteaza():
	if request.method == 'POST':
		nume = request.form['nume']
		parola = request.form['parola']
		if nume is None:
			flash('Numele este necesar.')
		elif parola is None:
			flash('Parola este necesara.')
		else:
			cont = get_cont_gestiune(nume)
			if cont is None:
				flash('Contul este gresit.')
			elif not check_password_hash(cont[2], parola):
				flash('Parola este gresita.')
			else:
				session.clear()
				session['id_cont_gestionare'] = cont[0]
				return redirect(url_for('vedere_index.pagina_index'))
	return render_template('vedere_autentificare/pagina_conecteaza.html')
	
@plan_vedere_autentificare.route('/deconecteaza')
def pagina_deconecteaza():
	session.clear()
	return redirect(url_for('vedere_index.pagina_index'))
	
def decoreaza_conectare_necesara(pagina):
	@functools.wraps(pagina)
	def pagina_impachetata(**kwargs):
		if get_utilizator_conectat() is None:
			return redirect(url_for('vedere_autentificare.pagina_conecteaza'))
		return pagina(**kwargs)
	return pagina_impachetata
