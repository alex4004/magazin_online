CREATE TABLE conturi_gestiune
(
	id SERIAL,
	nume VARCHAR(40) UNIQUE NOT NULL,
	hash_parola VARCHAR
);

CREATE TABLE stari_comenzi
(
	nume VARCHAR(40),
	PRIMARY KEY (nume)
);

CREATE TABLE comenzi
(
	id SERIAL,
	nume VARCHAR(40),
	email VARCHAR(40),
	telefon VARCHAR(40),
	adresa_livrare VARCHAR(40),
	pret_total DECIMAL(20,4),
	stare VARCHAR(40),
	FOREIGN KEY (stare) REFERENCES stari_comenzi(nume)
);

CREATE TABLE produse_comenzi
(
	id_comanda INTEGER,
	nume_produs VARCHAR(40),
	cantitate REAL
);
