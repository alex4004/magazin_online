from flask import Blueprint, request, url_for, render_template, redirect, g
from . import baze_de_date
from . import vedere_autentificare

plan_vedere_comenzi = Blueprint('vedere_comenzi', __name__, url_prefix='/comenzi')

def get_comenzi():
	if 'comenzi' not in g:
		with baze_de_date.get_baza_de_date_gestiune().cursor() as cursor:
			cursor.execute('SELECT id, nume, email, telefon, adresa_livrare, pret_total, stare FROM comenzi')
			g.comenzi = cursor.fetchall()
	return g.comenzi

def get_comanda(id_comanda):
	with baze_de_date.get_baza_de_date_gestiune().cursor() as cursor:
		cursor.execute('SELECT id, nume, email, telefon, adresa_livrare, pret_total, stare FROM comenzi WHERE id = %s', (id_comanda,))
		return cursor.fetchone()

def get_produse_comanda(id_comanda):
	with baze_de_date.get_baza_de_date_gestiune().cursor() as cursor:
		cursor.execute('SELECT nume_produs, cantitate FROM produse_comenzi WHERE id_comanda = %s', (id_comanda,))
		return cursor.fetchall()

@plan_vedere_comenzi.route('/', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_index():
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	get_comenzi()
	return render_template('vedere_comenzi/pagina_index.html')

@plan_vedere_comenzi.route('/<int:id_comanda>/detalii', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_detalii(id_comanda):
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	g.comanda = get_comanda(id_comanda)
	g.produse_comanda = get_produse_comanda(id_comanda)
	return render_template('vedere_comenzi/pagina_detalii.html')

@plan_vedere_comenzi.route('/<int:id_comanda>/marcheaza_livrata', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_marcheaza_livrata(id_comanda):
	conexiune = baze_de_date.get_baza_de_date_gestiune()
	with conexiune.cursor() as cursor:
		cursor.execute('UPDATE comenzi SET stare = %s WHERE id = %s', ('livrata', id_comanda))
	conexiune.commit()
	ic = id_comanda
	return redirect(url_for('vedere_comenzi.pagina_detalii', id_comanda=ic))

@plan_vedere_comenzi.route('/<int:id_comanda>/sterge', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_sterge(id_comanda):
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	g.comanda = get_comanda(id_comanda)
	if request.method == 'POST':
		conexiune = baze_de_date.get_baza_de_date_gestiune()
		with conexiune.cursor() as cursor:
			cursor.execute('DELETE FROM comenzi WHERE id = %s', (id_comanda,))
		conexiune.commit()
		return redirect(url_for('vedere_comenzi.pagina_index'))
	return render_template('vedere_comenzi/pagina_sterge.html')
