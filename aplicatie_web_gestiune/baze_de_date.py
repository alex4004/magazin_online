import sys, os
from time import sleep
from werkzeug.security import generate_password_hash
from flask import current_app, g
import psycopg2
from psycopg2 import sql

def conecteaza(adresa):
	return psycopg2.connect(host=adresa, database='postgres', user=str(os.getenv('NUME_CONT_GESTIUNE_BAZE_DE_DATE')),
		password=str(os.getenv('PAROLA_CONT_GESTIUNE_BAZE_DE_DATE')))
	
def baze_de_date_initializate():
	conexiune = get_baza_de_date_clienti()
	with conexiune.cursor() as cursor:
		cursor.execute('SELECT EXISTS(SELECT 1 FROM information_schema.tables WHERE table_schema = %s AND table_name = %s)',
			('public', 'informatii_magazin'))
		rezultat = cursor.fetchone()[0] == 1
	conexiune.commit()
	return rezultat

def initializeaza(conexiune, fisier):
	autocommit = conexiune.autocommit
	try:
		conexiune.autocommit = True
		with conexiune.cursor()	as cursor:
			with current_app.open_resource(fisier, 'r') as f:
				cursor.execute(f.read())
	finally:
		conexiune.autocommit = autocommit
		
def adauga_cont_gestiune_initial():
	conexiune = get_baza_de_date_gestiune()
	with conexiune.cursor() as cursor:
		cursor.execute('INSERT INTO conturi_gestiune(nume, hash_parola) VALUES (%s, %s)',
			(os.getenv('NUME_CONT_GESTIUNE_INITIAL'), generate_password_hash(str(os.getenv('PAROLA_CONT_GESTIUNE_INITIAL')))))
	conexiune.commit()

def adauga_cont_client():
	conexiune = get_baza_de_date_clienti()
	nume_cont = str(os.getenv('NUME_CONT_CLIENTI_PENTRU_BAZA_DE_DATE'))
	parola_cont = str(os.getenv('PAROLA_CONT_CLIENTI_PENTRU_BAZA_DE_DATE'))
	with conexiune.cursor() as cursor:
		cursor.execute(sql.SQL('CREATE USER {} WITH ENCRYPTED PASSWORD %s').format(sql.Identifier(nume_cont)), (parola_cont,))
		cursor.execute(sql.SQL('GRANT SELECT ON TABLE informatii_magazin, produse, stoc TO {}').format(sql.Identifier(nume_cont)))
	conexiune.commit()

def get_baza_de_date_gestiune():
	if 'baza_de_date_gestiune' not in g:
		g.baza_de_date_gestiune = conecteaza(current_app.config['ADRESA_BAZA_DE_DATE_GESTIUNE'])
	return g.baza_de_date_gestiune
	
def get_baza_de_date_clienti():
	if 'baza_de_date_clienti' not in g:
		g.baza_de_date_clienti = conecteaza(current_app.config['ADRESA_BAZA_DE_DATE_CLIENTI'])
	return g.baza_de_date_clienti
	
def get_informatii_magazin():
	if 'informatii_magazin' not in g:
		with get_baza_de_date_clienti().cursor() as cursor:
			cursor.execute('SELECT id, nume FROM informatii_magazin')
			g.informatii_magazin = cursor.fetchone()	
	return g.informatii_magazin
		
def initializeaza_baze_de_date():
	if 'baza_de_date_gestiune' not in g:
		try:
			g.baza_de_date_gestiune = conecteaza(current_app.config['ADRESA_BAZA_DE_DATE_GESTIUNE'])
		except Exception:
			pass
		while 'baza_de_date_gestiune' not in g:
			print('Se incearca conectarea la baza de date de gestiune.')
			sys.stdout.flush()
			sleep(1)
			try:
				g.baza_de_date_gestiune = conecteaza(current_app.config['ADRESA_BAZA_DE_DATE_GESTIUNE'])
			except Exception:
				pass
	if 'baza_de_date_clienti' not in g:
		try:
			g.baza_de_date_clienti = conecteaza(current_app.config['ADRESA_BAZA_DE_DATE_CLIENTI'])
		except Exception:
			pass
		while 'baza_de_date_clienti' not in g:
			print('Se incearca conectarea la baza de date pentru clienti.')
			sys.stdout.flush()
			sleep(1)
			try:
				g.baza_de_date_clienti = conecteaza(current_app.config['ADRESA_BAZA_DE_DATE_CLIENTI'])		
			except Exception:
				pass
	if not baze_de_date_initializate():
		print('Se initializeaza bazele de date.')
		sys.stdout.flush()
		initializeaza(g.baza_de_date_gestiune, 'schema_baza_de_date_gestiune.sql')
		initializeaza(g.baza_de_date_clienti, 'schema_baza_de_date_clienti.sql')
		initializeaza(g.baza_de_date_gestiune, 'initializare_baza_de_date_gestiune.sql')
		adauga_cont_gestiune_initial()
		initializeaza(g.baza_de_date_clienti, 'initializare_baza_de_date_clienti.sql')
		adauga_cont_client()
	
def deconecteaza_baze_de_date(e=None):
	baza_de_date_gestiune = g.pop('baza_de_date_gestiune', None)
	if baza_de_date_gestiune is not None:
		baza_de_date_gestiune.close()
	baza_de_date_clienti = g.pop('baza_de_date_clienti', None)
	if baza_de_date_clienti is not None:
		baza_de_date_clienti.close()
	
def initializeaza_modul_pentru_aplicatie(aplicatie):
	initializeaza_baze_de_date()	
	aplicatie.teardown_appcontext(deconecteaza_baze_de_date)

	
