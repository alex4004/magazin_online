from werkzeug.security import generate_password_hash
from flask import Blueprint, request, url_for, render_template, flash, redirect, g
from . import baze_de_date
from . import vedere_autentificare

plan_vedere_conturi_gestiune = Blueprint('vedere_conturi_gestiune', __name__, url_prefix='/conturi_gestiune')

def get_conturi_gestiune():
	if 'conturi_gestiune' not in g:
		with baze_de_date.get_baza_de_date_gestiune().cursor() as cursor:
			cursor.execute('SELECT Id, Nume, Hash_Parola FROM Conturi_Gestiune')
			g.conturi_gestiune = cursor.fetchall()
	return g.conturi_gestiune
	
def get_cont_gestiune(id):
	with baze_de_date.get_baza_de_date_gestiune().cursor() as cursor:
		cursor.execute('SELECT Id, Nume, Hash_Parola FROM Conturi_Gestiune WHERE Id = %s', (id,))
		return cursor.fetchone()
	
@plan_vedere_conturi_gestiune.route('/', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_index():
	baze_de_date.get_informatii_magazin()
	get_conturi_gestiune()
	vedere_autentificare.get_utilizator_conectat()
	return render_template('vedere_conturi_gestiune/pagina_index.html')

@plan_vedere_conturi_gestiune.route('/adauga', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_adauga():
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	if request.method == 'POST':
		nume = request.form['nume']
		parola = request.form['parola']
		if nume is None:
			flash('Numele este necesar.')
		elif parola is None:
			flash('Parola este necesara.')
		else:
			conexiune = baze_de_date.get_baza_de_date_gestiune()
			with conexiune.cursor() as cursor:
				cursor.execute('INSERT INTO conturi_gestiune(nume, hash_parola) VALUES (%s, %s)',
					(nume, generate_password_hash(parola)))
			conexiune.commit()
			return redirect(url_for('vedere_conturi_gestiune.pagina_index'))
	return render_template('vedere_conturi_gestiune/pagina_adauga.html')
	
@plan_vedere_conturi_gestiune.route('/<int:id>/modifica', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_modifica(id):
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	g.cont_gestiune = get_cont_gestiune(id)
	if request.method == 'POST':
		parola = request.form['parola']
		if parola is None:
			flash('Parola este necesara.')
		else:
			conexiune = baze_de_date.get_baza_de_date_gestiune()
			with conexiune.cursor() as cursor:
				cursor.execute('UPDATE conturi_gestiune SET hash_parola = %s WHERE id = %s',
					(generate_password_hash(parola), id))
			conexiune.commit()
			return redirect(url_for('vedere_conturi_gestiune.pagina_index'))
	return render_template('vedere_conturi_gestiune/pagina_modifica.html')
	
@plan_vedere_conturi_gestiune.route('/<int:id>/sterge', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_sterge(id):
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	g.cont_gestiune = get_cont_gestiune(id)
	if id == g.utilizator_conectat[0]:
		flash('Nu se poate sterge contul conectat in aceasta sesiune.')
	elif len(get_conturi_gestiune()) == 1:
		flash('Nu se poate sterge singurul cont de gestiune.')
	elif request.method == 'POST':
		conexiune = baze_de_date.get_baza_de_date_gestiune()
		with conexiune.cursor() as cursor:
			cursor.execute('DELETE FROM conturi_gestiune WHERE id = %s', (id,))
		conexiune.commit()
		return redirect(url_for('vedere_conturi_gestiune.pagina_index'))
	return render_template('vedere_conturi_gestiune/pagina_sterge.html')
	
