CREATE TABLE informatii_magazin
(
	id SERIAL,
	nume VARCHAR(40)
);

CREATE TABLE produse
(
	cod VARCHAR(40),
	nume VARCHAR(40),
	pret_unitar DECIMAL(20,4),
	PRIMARY KEY(cod)
);

CREATE TABLE stoc
(
	cod_produs VARCHAR(40),
	cantitate REAL,
	cantitate_suficienta REAL,
	PRIMARY KEY (cod_produs),
	FOREIGN KEY (cod_produs) REFERENCES produse(cod) ON DELETE CASCADE,
	CHECK (cantitate_suficienta > 0)
);


