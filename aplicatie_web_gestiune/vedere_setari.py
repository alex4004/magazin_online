from flask import Blueprint, request, url_for, render_template, flash, redirect
from . import baze_de_date
from . import vedere_autentificare

plan_vedere_setari = Blueprint('vedere_setari', __name__, url_prefix='/setari')

@plan_vedere_setari.route('/', methods=('GET', 'POST'))
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_index():
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	if request.method == 'POST':
		nume_magazin = request.form['nume_magazin']
		if nume_magazin is None:
			flash('Numele este necesar.')
		else:
			conexiune = baze_de_date.get_baza_de_date_clienti()
			with conexiune.cursor() as cursor:
				cursor.execute('UPDATE Informatii_Magazin SET Nume = %s', (nume_magazin,))
			conexiune.commit()
			return redirect(url_for('vedere_index.pagina_index'))
	return render_template('vedere_setari/pagina_index.html')
