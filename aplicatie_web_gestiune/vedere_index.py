from flask import Blueprint, render_template
from . import baze_de_date
from . import vedere_autentificare

plan_vedere_index = Blueprint('vedere_index', __name__)

@plan_vedere_index.route('/')
@vedere_autentificare.decoreaza_conectare_necesara
def pagina_index():
	baze_de_date.get_informatii_magazin()
	vedere_autentificare.get_utilizator_conectat()
	return render_template('vedere_index/pagina_index.html')
