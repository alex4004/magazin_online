package magazin_online.lucrator;

import java.lang.*;
import java.util.*;
import java.math.*;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import java.sql.*;
import org.json.*;

public class Lucrator
{
	static void dormi(long milisecunde)
	{
		try
		{
			Thread.sleep(milisecunde);
		}
		catch (InterruptedException e)
		{
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}

	static Jedis conecteazaLaRedis(String adresa)
	{
		Jedis conexiune = new Jedis(adresa);
		while (true)
		{
			try
			{
				conexiune.keys("*");
				break;
			}
			catch (JedisConnectionException e)
			{
				System.out.println("Se incearca conectarea la coada de comenzi ale clientilor.");
				dormi(1000);
			}
		}
		return conexiune;
	}

	static Connection conecteazaLaPostgreSQL(String nume, String adresa, String cont, String parola)
	{
		Connection conexiune = null;
		try
		{
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://" + adresa + "/postgres";
			while (conexiune == null)
			{
				try
				{
					conexiune = DriverManager.getConnection(url, cont, parola);
				}
				catch (SQLException e)
				{
					System.out.println("Se incearca conectarea la " + nume + ".");
					dormi(1000);
				}
			}
		}
		catch (ClassNotFoundException e)
		{
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
		return conexiune;
	}

	static Comanda parseazaComandaJson(String json)
	{
		JSONObject comandaJSONObject = new JSONObject(json);
		String nume = comandaJSONObject.getString("nume");
		String email = comandaJSONObject.getString("email");
		String telefon = comandaJSONObject.getString("telefon");
		String adresaLivrare = comandaJSONObject.getString("adresa_livrare");
		JSONArray produseLivrateJSONArray = comandaJSONObject.getJSONArray("produse_livrate");
		List<ProdusComanda> produseLivrate = new ArrayList<ProdusComanda>();
		for (int i = 0; i < produseLivrateJSONArray.length(); ++i)
		{
			JSONArray produsLivratJSONArray = produseLivrateJSONArray.getJSONArray(i);
			produseLivrate.add(new ProdusComanda(produsLivratJSONArray.getString(0),
				(float)produsLivratJSONArray.getDouble(1)));
		}
		return new Comanda(nume, email, telefon, adresaLivrare, produseLivrate);
	}

	static boolean completeazaNumeComanda(Connection bazaDeDateClienti, Comanda comanda) throws SQLException
	{
		boolean produseValide = true;
		List<ProdusComanda> produse = comanda.getProduse();
		for (int i = 0; i < produse.size(); ++i)
		{
			PreparedStatement comandaInterogareProdus = bazaDeDateClienti.prepareStatement
				("SELECT nume FROM produse WHERE cod = ?");
			ProdusComanda produs = produse.get(i);
			comandaInterogareProdus.setString(1, produs.getCod());
			ResultSet rezultatInterogareProdus = comandaInterogareProdus.executeQuery();
			if (!rezultatInterogareProdus.next())
			{
				produseValide = false;
				produs.setNume(String.format("Produs neinregistrat avand codul \"%s\"", produs.getCod()));
			}
			else
				produs.setNume(rezultatInterogareProdus.getString(1));
		}
		return produseValide;
	}

	static boolean verificaStocComanda(Connection bazaDeDateClienti, Comanda comanda) throws SQLException
	{
		List<ProdusComanda> produse = comanda.getProduse();
		for (int i = 0; i < produse.size(); ++i)
		{
			PreparedStatement comandaInterogareStoc = bazaDeDateClienti.prepareStatement
				("SELECT cantitate FROM stoc WHERE cod_produs = ?");
			ProdusComanda produs = produse.get(i);
			comandaInterogareStoc.setString(1, produs.getCod());
			ResultSet rezultatInterogareStoc = comandaInterogareStoc.executeQuery();
			float cantitateStoc = 0;
			if (rezultatInterogareStoc.next())
				cantitateStoc = rezultatInterogareStoc.getFloat(1);
			if (produs.getCantitate() > cantitateStoc)
				return false;
		}
		return true;
	}

	static void completeazaPretTotalComanda(Connection bazaDeDateClienti, Comanda comanda) throws SQLException
	{
		BigDecimal pretTotal = new BigDecimal(0);
		List<ProdusComanda> produse = comanda.getProduse();
		for (int i = 0; i < produse.size(); ++i)
		{
			PreparedStatement comandaInterogarePretUnitar = bazaDeDateClienti.prepareStatement
				("SELECT pret_unitar FROM produse WHERE cod = ?");
			ProdusComanda produs = produse.get(i);
			comandaInterogarePretUnitar.setString(1, produs.getCod());
			ResultSet rezultatInterogarePretUnitar = comandaInterogarePretUnitar.executeQuery();
			rezultatInterogarePretUnitar.next();
			pretTotal = pretTotal.add(rezultatInterogarePretUnitar.getBigDecimal(1).multiply(new BigDecimal(produs.getCantitate())));
		}
		comanda.setPretTotal(pretTotal);
	}

	static void consumaStoc(Connection bazaDeDateClienti, Comanda comanda) throws SQLException
	{
		List<ProdusComanda> produse = comanda.getProduse();
		for (int i = 0; i < produse.size(); ++i)
		{
			PreparedStatement comandaActualizareStoc = bazaDeDateClienti.prepareStatement
				("UPDATE stoc SET cantitate = cantitate - ? WHERE cod_produs = ?");
			ProdusComanda produs = produse.get(i);
			comandaActualizareStoc.setFloat(1, produs.getCantitate());
			comandaActualizareStoc.setString(2, produs.getCod());
			comandaActualizareStoc.executeUpdate();
		}
	}

	static void inregistreazaComanda(Connection bazaDeDateGestiune, Comanda comanda) throws SQLException
	{
		PreparedStatement comandaInregistrareComanda = bazaDeDateGestiune.prepareStatement
			("INSERT INTO comenzi(nume, email, telefon, adresa_livrare, pret_total, stare) VALUES (?, ?, ?, ?, ?, ?) RETURNING id");
		comandaInregistrareComanda.setString(1, comanda.getNume());
		comandaInregistrareComanda.setString(2, comanda.getEmail());
		comandaInregistrareComanda.setString(3, comanda.getTelefon());
		comandaInregistrareComanda.setString(4, comanda.getAdresaLivrare());
		comandaInregistrareComanda.setBigDecimal(5, comanda.getPretTotal());
		comandaInregistrareComanda.setString(6, Comanda.stringDinStare(comanda.getStare()));
		if (!comandaInregistrareComanda.execute())
			throw new IllegalStateException("Clauza de inserare nu a intors rezultatul necesar.");
		ResultSet rezultatInserare = comandaInregistrareComanda.getResultSet();
		if (!rezultatInserare.next())
			throw new IllegalStateException("Clauza de inserare nu a intors rezultatul necesar.");
		int idComanda = rezultatInserare.getInt(1);
		List<ProdusComanda> produse = comanda.getProduse();
		for (int i = 0; i < produse.size(); ++i)
		{
			PreparedStatement comandaInregistrareProduseComanda = bazaDeDateGestiune.prepareStatement
				("INSERT INTO produse_comenzi(id_comanda, nume_produs, cantitate) VALUES (?, ?, ?)");
			ProdusComanda produs = produse.get(i);
			comandaInregistrareProduseComanda.setInt(1, idComanda);
			comandaInregistrareProduseComanda.setString(2, produs.getNume());
			comandaInregistrareProduseComanda.setFloat(3, produs.getCantitate());
			comandaInregistrareProduseComanda.executeUpdate();
		}
	}

	public static void main(String[] args)
	{
		try
		{
			Jedis coadaComenziClienti = conecteazaLaRedis("coada_comenzi_clienti");
			Map<String, String> variabileDeMediu = System.getenv();
			Connection bazaDeDateGestiune = conecteazaLaPostgreSQL("baza de date gestiune", "baza_de_date_gestiune",
				variabileDeMediu.get("NUME_CONT_GESTIUNE_BAZE_DE_DATE"),
				variabileDeMediu.get("PAROLA_CONT_GESTIUNE_BAZE_DE_DATE"));
			Connection bazaDeDateClienti = conecteazaLaPostgreSQL("baza de date clienti", "baza_de_date_clienti",
				variabileDeMediu.get("NUME_CONT_GESTIUNE_BAZE_DE_DATE"),
				variabileDeMediu.get("PAROLA_CONT_GESTIUNE_BAZE_DE_DATE"));
			while (true)
			{
				String jsonComanda = coadaComenziClienti.blpop(0, "comenzi").get(1);
				Comanda comanda = parseazaComandaJson(jsonComanda);
				if (!completeazaNumeComanda(bazaDeDateClienti, comanda))
					comanda.setStare(Comanda.Stare.INVALIDA);
				else
				{
					completeazaPretTotalComanda(bazaDeDateClienti, comanda);
					if (!verificaStocComanda(bazaDeDateClienti, comanda))
						comanda.setStare(Comanda.Stare.STOC_INSUFICIENT);
					else
					{
						comanda.setStare(Comanda.Stare.NELIVRATA);
						consumaStoc(bazaDeDateClienti, comanda);
					}
				}
				inregistreazaComanda(bazaDeDateGestiune, comanda);
			}
        }
		catch (SQLException e)
		{
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}
}
