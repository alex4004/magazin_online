package magazin_online.lucrator;

class ProdusComanda
{
	private String cod;
	private String nume;
	private float cantitate;

	public ProdusComanda(String cod, float cantitate)
	{
		this.cod = cod;
		this.cantitate = cantitate;
	}

	public String getCod()
	{
		return cod;
	}

	public String getNume()
	{
		return nume;
	}

	public void setNume(String nume)
	{
		this.nume = nume;
	}

	public float getCantitate()
	{
		return cantitate;
	}
}
