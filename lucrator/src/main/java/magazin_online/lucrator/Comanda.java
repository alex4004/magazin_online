package magazin_online.lucrator;

import java.lang.*;
import java.util.*;
import java.math.*;

public class Comanda
{
	private String nume;
	private String email;
	private String telefon;
	private String adresaLivrare;
	private List<ProdusComanda> produse;
	private BigDecimal pretTotal;
	private Stare stare;

	public Comanda(String nume, String email, String telefon, String adresaLivrare, List<ProdusComanda> produse)
	{
		this.nume = nume;
		this.email = email;
		this.telefon = telefon;
		this.adresaLivrare = adresaLivrare;
		this.produse = produse;
	}

	public String getNume()
	{
		return nume;
	}

	public String getEmail()
	{
		return email;
	}

	public String getTelefon()
	{
		return telefon;
	}

	public String getAdresaLivrare()
	{
		return adresaLivrare;
	}

	public List<ProdusComanda> getProduse()
	{
		return produse;
	}

	public BigDecimal getPretTotal()
	{
		return pretTotal;
	}

	public void setPretTotal(BigDecimal pretTotal)
	{
		this.pretTotal = pretTotal;
	}

	public Stare getStare()
	{
		return stare;
	}

	public void setStare(Stare stare)
	{
		this.stare = stare;
	}

	enum Stare
	{
		INVALIDA,
		STOC_INSUFICIENT,
		NELIVRATA,
		LIVRATA
	}

	static Stare stareDinString(String string)
	{
		if (string.equals("invalida"))
			return Stare.INVALIDA;
		if (string.equals("stoc_insuficient"))
			return Stare.STOC_INSUFICIENT;
		if (string.equals("nelivrata"))
			return Stare.NELIVRATA;
		if (string.equals("livrata"))
			return Stare.LIVRATA;
		throw new IllegalArgumentException("String-ul nu reprezinta o stare a unei comenzi");
	}

	static String stringDinStare(Stare stare)
	{
		if (stare == Stare.INVALIDA)
			return "invalida";
		if (stare == Stare.STOC_INSUFICIENT)
			return "stoc_insuficient";
		if (stare == Stare.NELIVRATA)
			return "nelivrata";
		return "livrata";
	}
}
