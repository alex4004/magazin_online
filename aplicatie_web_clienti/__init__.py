import os
from flask import Flask
from . import baze_de_date
from . import vedere_index
from . import vedere_cumparare

def configureaza_implicit_aplicatie(aplicatie):
	aplicatie.config['SECRET_KEY']=str(os.getenv('CHEIE_CRIPTARE_COOKIES_APLICATIE_WEB_CLIENTI'))
	aplicatie.config['ADRESA_BAZA_DE_DATE_CLIENTI']='baza_de_date_clienti'
	aplicatie.config['ADRESA_COADA_COMENZI_CLIENTI']='coada_comenzi_clienti'

def creeaza_aplicatie():
	aplicatie = Flask(__name__, instance_relative_config=True)
	instance_path = aplicatie.instance_path
	try:
		os.makedirs(instance_path)
	except OSError:
		if not os.access(instance_path, os.R_OK | os.W_OK):
			raise
	configureaza_implicit_aplicatie(aplicatie)
	with aplicatie.app_context():
		baze_de_date.initializeaza_modul_pentru_aplicatie(aplicatie)
		aplicatie.register_blueprint(vedere_index.plan_vedere_index)
		aplicatie.add_url_rule('/', endpoint='vedere_index.pagina_index')
		aplicatie.register_blueprint(vedere_cumparare.plan_vedere_cumparare)
	return aplicatie
