from flask import Blueprint, render_template, g
from . import baze_de_date

plan_vedere_index = Blueprint('vedere_index', __name__)

def get_produse():
	if 'produse' not in g:
		with baze_de_date.get_baza_de_date_clienti().cursor() as cursor:
			cursor.execute('SELECT cod, nume, pret_unitar FROM produse')
			g.produse = cursor.fetchall()
	return g.produse

def get_stoc_produs(cod_produs):
	with baze_de_date.get_baza_de_date_clienti().cursor() as cursor:
		cursor.execute('SELECT cod_produs, cantitate, cantitate_suficienta FROM stoc WHERE cod_produs = %s', (cod_produs,))
		stoc = cursor.fetchone()
		if stoc is None:
			return (cod_produs, 0, 1)
		return stoc

def get_stoc():
	if 'stoc' not in g:
		get_produse()
		g.stoc = {}
		for produs in g.produse:
			g.stoc[produs[0]] = get_stoc_produs(produs[0])
	return g.stoc

@plan_vedere_index.route('/', methods=('GET', 'POST'))
def pagina_index():
	baze_de_date.get_informatii_magazin()
	get_produse()
	get_stoc()
	return render_template('vedere_index/pagina_index.html')
