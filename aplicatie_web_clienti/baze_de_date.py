import sys, os
from time import sleep
from flask import current_app, g
import psycopg2

def conecteaza(adresa):
	return psycopg2.connect(host=adresa, database='postgres', user=str(os.getenv('NUME_CONT_CLIENTI_PENTRU_BAZA_DE_DATE')),
		password=str(os.getenv('PAROLA_CONT_CLIENTI_PENTRU_BAZA_DE_DATE')))

def get_baza_de_date_clienti():
	if 'baza_de_date_clienti' not in g:
		g.baza_de_date_clienti = conecteaza(current_app.config['ADRESA_BAZA_DE_DATE_CLIENTI'])
	return g.baza_de_date_clienti

def get_informatii_magazin():
	if 'informatii_magazin' not in g:
		with get_baza_de_date_clienti().cursor() as cursor:
			cursor.execute('SELECT id, nume FROM informatii_magazin')
			g.informatii_magazin = cursor.fetchone()
	return g.informatii_magazin

def asteapta_baza_de_date():
	if 'baza_de_date_clienti' not in g:
		try:
			g.baza_de_date_clienti = conecteaza(current_app.config['ADRESA_BAZA_DE_DATE_CLIENTI'])
		except Exception:
			pass
		while 'baza_de_date_clienti' not in g:
			print('Se incearca conectarea la baza de date pentru clienti.')
			sys.stdout.flush()
			sleep(1)
			try:
				g.baza_de_date_clienti = conecteaza(current_app.config['ADRESA_BAZA_DE_DATE_CLIENTI'])
			except Exception:
				pass

def deconecteaza_baza_de_date(e=None):
	baza_de_date_clienti = g.pop('baza_de_date_clienti', None)
	if baza_de_date_clienti is not None:
		baza_de_date_clienti.close()

def initializeaza_modul_pentru_aplicatie(aplicatie):
	asteapta_baza_de_date()
	aplicatie.teardown_appcontext(deconecteaza_baza_de_date)
