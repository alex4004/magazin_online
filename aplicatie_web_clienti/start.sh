#!/bin/bash
if [ -z ${MOD_DEPANARE_APLICATIE+x} ]
then #mod principal
	waitress-serve --port 80 --call aplicatie:creeaza_aplicatie
else #mod depanare
	export FLASK_APP=aplicatie:creeaza_aplicatie
	export FLASK_ENV=development
	export FLASK_RUN_PORT=80
	flask run --host=0.0.0.0
fi
