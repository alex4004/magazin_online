from flask import Blueprint, request, url_for, render_template, flash, redirect, g
import json
from redis import Redis
from . import baze_de_date
from . import vedere_index
from . import coada_comenzi_clienti

plan_vedere_cumparare = Blueprint('vedere_cumparare', __name__, url_prefix='/cumparare')

@plan_vedere_cumparare.route('/', methods=('GET', 'POST'))
def pagina_index():
	baze_de_date.get_informatii_magazin()
	if request.method == 'POST':
		nr_produse_diferite = request.form['numar_produse_diferite']
		if nr_produse_diferite is None:
			flash('Numarul de produse diferite este necesar.')
		else:
			try:
				nr_produse_diferite = int(nr_produse_diferite)
				if nr_produse_diferite <= 0:
					flash('Numarul de produse diferite nu este valid.')
				else:
					return redirect(url_for('vedere_cumparare.pagina_introducere_produse', numar_produse_diferite=nr_produse_diferite))
			except Exception:
				flash('Numarul de produse diferite nu este formatat corect.')
	return render_template('vedere_cumparare/pagina_index.html')

@plan_vedere_cumparare.route('/<int:numar_produse_diferite>/introducere_produse', methods=('GET', 'POST'))
def pagina_introducere_produse(numar_produse_diferite):
	baze_de_date.get_informatii_magazin()
	vedere_index.get_produse()
	vedere_index.get_stoc()
	produse_existente = set()
	for produs in g.produse:
		produse_existente.add(produs[0])
	g.numar_produse_diferite = numar_produse_diferite
	if numar_produse_diferite <= 0:
		flash('Numarul de produse diferite nu este valid (aceasta pagina este invalida).')
	elif request.method == 'POST':
		comanda = {}
		comanda['nume'] = request.form['nume']
		comanda['email'] = request.form['email']
		comanda['telefon'] = request.form['telefon']
		comanda['adresa_livrare'] = request.form['adresa_livrare']
		if comanda['nume'] is None:
			flash('Numele este necesar.')
		elif comanda['email'] is None:
			flash('Emailul este necesar.')
		elif comanda['telefon'] is None:
			flash('Telefonul este necesar.')
		elif comanda['adresa_livrare'] is None:
			flash('Adresa de livrare este necesara.')
		else:
			comanda['produse_livrate'] = []
			eroare = False
			for i in range(1, numar_produse_diferite+1):
				cod = request.form['cod_produs_{}'.format(i)]
				cantitate = request.form['cantitate_produs_{}'.format(i)]
				if cod is None:
					flash('Codul produsului {} este necesar.'.format(i))
					eroare = True
					break
				elif cantitate is None:
					flash('Cantitatea produsului {} este necesara.'.format(i))
					eroare = True
					break
				else:
					try:
						cantitate=float(cantitate)
						if cantitate <= 0:
							flash('Cantitatea produsului {} nu este valida.'.format(i))
							eroare = True
							break
						elif cod not in produse_existente:
							flash('Nu exista produs inregistrat avand codul {} (codul produsului {}).'.format(cod, i))
							eroare = True
							break
						elif cantitate > g.stoc[cod][1]:
							flash('Stocul produsului care are codul {} este insuficient.'.format(i))
							eroare = True
							break
						else:
							comanda['produse_livrate'].append((cod, cantitate))
					except Exception:
						flash('Cantitatea produsului {} nu este formatata corect.'.format(i))
						eroare = True
						break
			if not eroare:
				coada_comenzi_clienti.get_coada_comenzi_clienti().rpush('comenzi', json.dumps(comanda))
				return redirect(url_for('vedere_index.pagina_index'))
	return render_template('vedere_cumparare/pagina_introducere_produse.html')
