from redis import Redis
from flask import current_app, g

def conecteaza(adresa):
	return Redis(host=adresa, db=0, socket_timeout=10)

def get_coada_comenzi_clienti():
	if 'coada_comenzi_clienti' not in g:
		g.coada_comenzi_clienti = conecteaza(current_app.config['ADRESA_COADA_COMENZI_CLIENTI'])
	return g.coada_comenzi_clienti
