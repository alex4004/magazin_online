Proiectul reprezinta un magazin online compus din module Docker.
Serverul de gestiune ruleaza pe portul 4000 si cel pentru clienti pe portul 80.
In fisierul /.env pot fi configurate modul de depanare, contul initial de
gestiune al magazinului, contul initial al bazelor de date (folosit pentru
administrare), contul initial folosit de aplicatia web destinata clientilor si 
cheile de criptare pentru cookies (cea a aplicatiei de gestiune si cea a aplicatiei 
destinata clientilor).
Fisierul este citit de Docker Compose. Modul de depanare poate fi activat
decomentand definitia variabilei de mediu MOD_DEPANARE_APLICATII. Contul format
din NUME_CONT_GESTIUNE_BAZE_DE_DATE si PAROLA_CONT_GESTIUNE_BAZE_DE_DATE este
folosit de aplicatia web de gestiune la fiecare conectare la bazele de date in timp
ce contul format din NUME_CONT_CLIENTI_PENTRU_BAZA_DE_DATE si 
PAROLA_CONT_CLIENTI_PENTRU_BAZA_DE_DATE este folosit de aplicatia web pentru clienti
la fiecare conectare la baza de date pentru clienti.
Parolele introduse pe site sunt trimise textual. Pentru securitatea datelor trimise
este necesara o conexiune criptata. Cheile de criptare pentru cookies trebuie setate
la valori aleatorii la pornirea initiala a serverelor. Modificarea lor ulterioara
invalideaza sesiunile existente.
Cantitatea necesara a unui produs reprezinta cantitatea care trebuie sa fie prezenta in
stoc pentru a afisa mesajul "in stoc" pe site-ul magazinului destinat clientilor.
